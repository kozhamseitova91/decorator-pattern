package com.company;

public abstract class MakeUp implements Face {
    public Face putMakeUp;

    public MakeUp(Face putMakeUp){
        this.putMakeUp = putMakeUp;
    }

    public void putMakeUp(){
        putMakeUp.putMakeUp();
    }
}